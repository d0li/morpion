from player import Player


class HumanPlayer(Player):
    def __init__(self, letter):
        super().__init__(letter)



    def get_move(self, game):
        """
        
        Returns
        -------
        The value of the square we want to play
        
        Description 
        -------
        Use bool to make sure no wrong value pass through
        ask user input
        check if correct value => if not => error message
        if ok => return value
        
        """
        valid_square = False
        val = None
        while not valid_square:
            square = input(self.letter + '\'s turn. Input move (0-8): ')
            val = int(square)
            if val not in game.available_moves():
                print('Invalid square. Try again.')
            valid_square = True
        return val