from player import Player
import random
import math

class RandomComputerPlayer(Player):

    def __init__(self, letter):
        super().__init__(letter)




    def get_move(self, board):
        """
        
        Parameters
        -------
        Current state of board game object
        
        Description
        -------
        choose a random square in the available_moves list
        
        """
        square = random.choice(board.available_moves())
        return square



class BitSmartComputerPlayer(Player):
    def __init__(self, letter):
        super().__init__(letter)


        
    def get_move(self, board):
        """
        
        Returns 
        -------
        square index for make_move

        Description
        -------
        will check if opponent can win next round and try to prevent it
        else play random
        
        """
        if len(self.check_winner(board, self.letter)) > 0:
            square = random.choice(self.check_winner(board, self.letter))
        else : 
            square =  random.choice(board.available_moves())
        return square
            
        
        
    
    def check_winner(self, board, player):
        """
        
        Returns 
        -------
        list of moves that can make other player loose

        Description
        -------
        Assign the right letter to test the winner condition
        Test all the moves for winning condition check
        Appends the moves list if its the case and reset current_winner to none
        Undo move whatever the result
        
        """
        couldloose = []
        other_player = 'O' if player == 'X' else 'X'
        for possible_move in board.available_moves():
            board.make_move(possible_move, other_player)
            if board.current_winner == other_player:
                board.board[possible_move] = ' '
                board.current_winner = None
                couldloose.append(possible_move)
            board.board[possible_move] = ' '
        return couldloose
        
        
    
    
class MinmaxComputerPlayer(Player):
    
    def __init__(self, letter):
        super().__init__(letter)



    def get_move(self, board):
        """
        
        Returns
        -------
        square index of optimal move
        
        Description
        -------
        If MinimaxComputerPlayer first player => First move = random move
        else, calling the minimax function to get the optimal move
        
        """
        if len(board.available_moves()) == 9:
            square = random.choice(board.available_moves())
        else:
            square = self.minimax(board, self.letter)['position']
        return square

   
    

    def minimax(self, board, player):
        """
        
        Returns
        -------
        
        Return the best move
        
        Description
        -------
        
        First we want to check if the game is win or a tie, and return the value of it
        We then assign -inf to ia player (who is maximizing)
        and inf to the other player (who is minimizing)       
        We'll then iterate in every move possible

        """
        max_player = self.letter  # yourself
        other_player = 'O' if player == 'X' else 'X'

        if board.current_winner == other_player:
            return {'position': None, 'score': 1 if other_player == max_player else -1}
        elif not board.empty_squares():
            return {'position': None, 'score': 0}

        if player == max_player:
            best = {'position': None, 'score': -math.inf}
        else:
            best = {'position': None, 'score': math.inf}
        for possible_move in board.available_moves():
            """
            
            Description
            -------
            Then for every move possible, we get the score of it by calling minimax again
            Then we undo the  and set winner to none (in case of a winner)
            Then we assign the possible move to the sim score position
            we check if the score of that sim_score is better than our actual score
            we return it so it can climb "the tree" back up            
            
            """
            board.make_move(possible_move, player)
            sim_score = self.minimax(board, other_player)


            board.board[possible_move] = ' '
            board.current_winner = None
            sim_score['position'] = possible_move 

            if player == max_player:
                if sim_score['score'] > best['score']:
                    best = sim_score
            else:
                if sim_score['score'] < best['score']:
                    best = sim_score
        return best




