import time
from computer import *
from human import *

class Board():

    def __init__(self):
        """
        
        Description:
        -------
        Create board object with no winner
        
        """
        self.board = self.make_board()
        self.current_winner = None




    @staticmethod
    def make_board():
        """
        
        Return
        -------
        fully blank board of 9 "boxes"
        
        """
        return [' ' for _ in range(9)]




    def print_board(self):
        """
        
        Returns
        -------
        Print the board
        
        """
        gameboard = "┌───┬───┬───┐ \n│ {0} │ {1} │ {2} │ (Position respective 0, 1, 2)\n├───┼───┼───┤\n│ {3} │ {4} │ " \
                    "{5} │ (Position respective 3, 4, 5)\n├───┼───┼───┤\n│ {6} │ {7} │ {8} │ (Position respective 6, " \
                    "7, 8)\n└───┴───┴───┘".format(self.board[0], self.board[1], self.board[2], self.board[3],
                                                  self.board[4], self.board[5], self.board[6], self.board[7],
                                                  self.board[8])
        print(gameboard)




    def make_move(self, square, letter):
        """
        
        Parameters
        -------
        letter: player that's playing
        square: the position we want to play on
    
        """
        if self.board[square] == ' ':
            self.board[square] = letter
            if self.winner(letter):
                self.current_winner = letter
            return True
        return False




    def winner(self, letter):
        """
        
        Parameters
        -------
        letter: letter we just played
   
        Returns
        ------- 
        bool that say if letter win or not
        
        """
        row1 = [self.board[i] for i in range (0, 3)]
        row2 = [self.board[i] for i in range (3, 6)]
        row3 = [self.board[i] for i in range (6, 9)]
        column1 = [self.board[i] for i in range (0, 7, 3)]
        column2 = [self.board[i] for i in range (1, 8, 3)]
        column3 = [self.board[i] for i in range (2, 9, 3)]
        diag1 = [self.board[i] for i in range (0, 9, 4)]
        diag2 = [self.board[i] for i in range (2, 7, 2)]
        
        if all([s == letter for s in row1]):
            return True
        elif all([s == letter for s in row2]):
            return True
        elif all([s == letter for s in row3]):
            return True
        elif all([s == letter for s in column1]):
            return True
        elif all([s == letter for s in column2]):
            return True
        elif all([s == letter for s in column3]):
            return True
        elif all([s == letter for s in diag1]):
            return True
        elif all([s == letter for s in diag2]):
            return True
        else:
            return False




    def available_moves(self):
        """
        
        Returns
        -------
        return available square index
        
        """
        return [i for i, x in enumerate(self.board) if x == " "]




    def empty_squares(self):
        """
        
        Returns
        ------- 
        return true if empty squares left
        
        """
        return ' ' in self.board
    



def play(game, x_player, o_player):
    """
    
    Parameters
    ----------
    game : current state of the game     
    x_player : computer or human object
    o_player : computer or human object

    Returns
    -------
    letter if winner, message if tie
    
    Description
    -------
    Always starting by "X"
    Game is on till we got either a winner or no square left
    Change of player at the end of each make_move function
    Check for winner at every move. If found, break loop
    Time.sleep function in case of IA to see it play

    """


    letter = 'X'
    while game.empty_squares():
        if letter == 'O':
            square = o_player.get_move(game)
        else:
            square = x_player.get_move(game)
        if game.make_move(square, letter):

            print(letter + ' makes a move to square {}'.format(square))
            game.print_board()
            print('')

            if game.current_winner:
                print(letter + ' wins!')
                return letter 
            
            if letter == 'X':
                letter = 'O'
            else:
                letter = 'X'


        time.sleep(0.8)


    print('It\'s a tie!')




if __name__ == '__main__':
    """
    
    Description
    -------
    keepG : boolean to keep playing at the end of first game
    goodA : good answers to the player selection, control by good (bool) inside while loop
    in the while loop => player assignement accrodingly to the user choice
    Creation of board object and game launch    
    
    """
    
    keepG = True
    goodA = [1, 2, 3, 4]
    
    
    while keepG :
        good = False
        while not good :
            ans = int(input("Welcome to the game. Whom should play? (first pick = first move) \n 1: Human \n 2: Computer (ez mode) \n 3: Computer (mid mode) \n 4: Computer (fuck u mode) \n"))
            if ans in goodA :
                if ans == 1 :
                    x_player = HumanPlayer('X')
                    good = True
                elif ans == 2 :
                    x_player = RandomComputerPlayer('X')
                    good = True
                elif ans == 3 :
                    x_player = BitSmartComputerPlayer('X')
                    good = True
                elif ans == 4 :
                    x_player = MinmaxComputerPlayer('X')
                    good = True
                
            else : 
                print("Your answer is incorrect, try again please.")
        
        good = False
        while not good :
            ans2 = int(input("Who shall it play againt? \n 1: Human \n 2: Computer (ez mode) \n 3: Computer (mid mode) \n 4: Computer (fuck u mode) \n"))
            
            if ans in goodA :
                if ans2 == 1 :
                    o_player = HumanPlayer('O')
                    good = True
                elif ans2 == 2 :
                    o_player = RandomComputerPlayer('O')
                    good = True
                elif ans2 == 3 :
                    o_player = BitSmartComputerPlayer('O')
                    good = True
                elif ans2 == 4 :
                    o_player = MinmaxComputerPlayer('O')
                    good = True
            else : 
                print("Your answer is incorrect, try again please.")
                    
        
        t = Board()
        play(t, x_player, o_player)
        
        keep = input("Do you want to play again? 'y' to continue, anything else to quit \n").lower()
        if keep != 'y':
            keepG = False